package cl.kotlinsantiago.kotlinroom.bd.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class UsuarioModel(
        @PrimaryKey(autoGenerate = true) val id : Long,
        val nombres: String,
        val apellidos: String,
        val email: String,
        val clave: String
)

/*
@Entity(tableName = "usuario")
data class UsuarioModel(
        @PrimaryKey(autoGenerate = true) val id : Long,
        @ColumnInfo(name = "nombre_compuesto") val nombres: String,
        @ColumnInfo(name = "apellidos") val apellidos: String,
        @ColumnInfo(name = "email") val email: String,
        @ColumnInfo(name = "email") val clave: String
)
* */

