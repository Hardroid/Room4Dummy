package cl.kotlinsantiago.kotlinroom.bd.dao

import android.arch.persistence.room.*
import cl.kotlinsantiago.kotlinroom.bd.model.UsuarioModel
import io.reactivex.Flowable

@Dao
interface UsuarioDao {
    @Query("SELECT * FROM UsuarioModel")
    fun obtenerUsuarios() : List<UsuarioModel>

    @Query("SELECT * FROM UsuarioModel")
    fun obtenerUsuariosRx() : Flowable<List<UsuarioModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertaUsuario(usuarioModel: UsuarioModel) : Long

    @Query("DELETE from UsuarioModel")
    fun deleteAll()

    @Update
    fun updateUsuario(usuarioModel: UsuarioModel)
}