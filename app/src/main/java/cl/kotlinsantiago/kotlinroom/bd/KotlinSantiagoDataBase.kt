package cl.kotlinsantiago.kotlinroom.bd

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import cl.kotlinsantiago.kotlinroom.bd.dao.UsuarioDao
import cl.kotlinsantiago.kotlinroom.bd.model.UsuarioModel

@Database(
        entities = arrayOf(
                UsuarioModel::class
        ),
        version = 1
)
abstract class KotlinSantiagoDataBase : RoomDatabase(){
    abstract fun usuarioDao(): UsuarioDao
}