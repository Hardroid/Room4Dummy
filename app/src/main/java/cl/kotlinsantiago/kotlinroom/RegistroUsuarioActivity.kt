package cl.kotlinsantiago.kotlinroom

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TextInputEditText
import android.view.View
import android.widget.Toast
import cl.kotlinsantiago.kotlinroom.bd.model.UsuarioModel
import kotlinx.android.synthetic.main.activity_registro_usuario.*

class RegistroUsuarioActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var mDbWorkerThread: DbWorkerThread
    private val mUiHandler = Handler()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro_usuario)
        btnAgregar.setOnClickListener(this)

        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()
    }

    override fun onClick(p0: View?) {
        if (isValidForm()) {
            guardaRegistro()
        } else {
            Toast.makeText(this, "Ingrese todos los datos", Toast.LENGTH_LONG).show()
        }
    }

    private fun obtenerUsuario(): UsuarioModel {
        return UsuarioModel(
                0,
                etNombre.text.toString(),
                etApellido.text.toString(),
                etEmail.text.toString(),
                etClave.text.toString())
    }

    private fun isValidForm(): Boolean {
        return !isEmptyText(etNombre) &&
                !isEmptyText(etApellido) &&
                !isEmptyText(etClave) &&
                !isEmptyText(etEmail)
    }

    private fun isEmptyText(etValue: TextInputEditText?): Boolean {
        return etValue?.text.toString().isEmpty()
    }

    fun guardaRegistro() {
        val task = Runnable {
            val id =
                    KotlinSantiagoApplication.database!!.usuarioDao().insertaUsuario(obtenerUsuario())
            mUiHandler.post({
                muestraIdRegistro(id)
            })
        }
        mDbWorkerThread.postTask(task)
    }

    private fun muestraIdRegistro(id: Long) {
        Toast.makeText(this, "Registro id: $id :)", Toast.LENGTH_LONG).show()
    }

    override fun onDestroy() {
        mDbWorkerThread.quit()
        super.onDestroy()
    }

}
