package cl.kotlinsantiago.kotlinroom

import android.os.AsyncTask
import cl.kotlinsantiago.kotlinroom.bd.model.UsuarioModel

class SelectTableAsyncTask : AsyncTask<Void, Void, List<UsuarioModel>> {

    val callBack: SelectCallBack

    constructor(callBack: SelectCallBack){
        this.callBack = callBack
    }

    override fun doInBackground(vararg p0: Void?): List<UsuarioModel> {
        return KotlinSantiagoApplication.database!!.usuarioDao().obtenerUsuarios()
    }

    override fun onPostExecute(result: List<UsuarioModel>) {
        super.onPostExecute(result)
        callBack.onResult(result)
    }

    interface SelectCallBack{
        fun onResult(result: List<UsuarioModel>)
    }
}