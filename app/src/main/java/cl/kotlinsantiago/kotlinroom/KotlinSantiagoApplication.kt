package cl.kotlinsantiago.kotlinroom

import android.app.Application
import android.arch.persistence.room.Room
import cl.kotlinsantiago.kotlinroom.bd.KotlinSantiagoDataBase

class KotlinSantiagoApplication : Application() {

    companion object {
        var database: KotlinSantiagoDataBase? = null
    }

    override fun onCreate() {
        super.onCreate()
        KotlinSantiagoApplication.database = Room.databaseBuilder(
                this,
                KotlinSantiagoDataBase::class.java,
                "kotlin.santiago-db").build()
    }
}