package cl.kotlinsantiago.kotlinroom

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cl.kotlinsantiago.kotlinroom.bd.model.UsuarioModel
import kotlinx.android.synthetic.main.usuario_item.view.*

class ListarUsuariosAdapter(val items: List<UsuarioModel>, val context: Context) : RecyclerView.Adapter<ListarUsuariosAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.usuario_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val productosValue = items.get(position)
        holder.bindData(productosValue)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtId = view.txtId
        val txtNombre = view.txtNombre
        val txtApellido = view.txtApellido
        val txtRut = view.txtEmail

        fun bindData(value : UsuarioModel){
            txtId?.text = "${value.id} )"
            txtNombre?.text = value.nombres
            txtApellido?.text = value.apellidos
            txtRut?.text = value.email
        }
    }
}


