package cl.kotlinsantiago.kotlinroom

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import cl.kotlinsantiago.kotlinroom.bd.model.UsuarioModel
import kotlinx.android.synthetic.main.activity_listar_usuarios.*
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.Ref
import org.jetbrains.anko.coroutines.experimental.asReference
import org.jetbrains.anko.coroutines.experimental.bg

class ListadoUsuariosCoroutineActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_usuarios)

        rvListadoUsuarios.layoutManager = LinearLayoutManager(this)
        fab.setOnClickListener(this)

        //Acá llamamos al la base de datos pero necesitamos un hilo
        //Para este ejemplo utilizaremos 3 formas
        //Coroutines de Kotlin
        //La vieja y confiable AsynTask
        //RxJava

    }

    override fun onResume() {
        super.onResume()
        //Se debe llamar en el onResume para que se actualice la tabla cuando ingresemos nuevos registros
        obtenerDatosConCoroutines()
    }

    private fun obtenerDatosConCoroutines() {

        async(UI) {
            val usuarios: Deferred<List<UsuarioModel>> = bg {
                KotlinSantiagoApplication.database!!.usuarioDao().obtenerUsuarios()
            }

            muestraUsuarios(usuarios.await())
        }
    }

    fun muestraUsuarios(usuarios: List<UsuarioModel>) {
        rvListadoUsuarios.adapter = ListarUsuariosAdapter(usuarios, this)
    }

    override fun onClick(p0: View?) {
        val intent = Intent(this, RegistroUsuarioActivity::class.java)
        startActivity(intent)
    }


}
