package cl.kotlinsantiago.kotlinroom

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import cl.kotlinsantiago.kotlinroom.bd.model.UsuarioModel
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_listar_usuarios.*
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.newCoroutineContext
import org.jetbrains.anko.coroutines.experimental.Ref
import org.jetbrains.anko.coroutines.experimental.asReference
import org.jetbrains.anko.coroutines.experimental.bg

class ListadoUsuariosRxActivity : AppCompatActivity(), View.OnClickListener {

    private val disposable: CompositeDisposable

    init {
        this.disposable = CompositeDisposable()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar_usuarios)

        rvListadoUsuarios.layoutManager = LinearLayoutManager(this)
        fab.setOnClickListener(this)

        //Acá llamamos al la base de datos pero necesitamos un hilo
        //Para este ejemplo utilizaremos 3 formas
        //Coroutines de Kotlin
        //La vieja y confiable AsynTask
        //RxJava
        obtenerUsuarios()

    }

    private fun obtenerUsuarios() {

        disposable.add(
                KotlinSantiagoApplication.database!!
                        .usuarioDao()
                        .obtenerUsuariosRx()
                        .toObservable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(SelectUsuariosDisposableObserver())

        )
    }

    fun muestraUsuarios(usuarios: List<UsuarioModel>) {
        rvListadoUsuarios.adapter = ListarUsuariosAdapter(usuarios, this)
    }

    override fun onClick(p0: View?) {
        val intent = Intent(this, RegistroUsuarioActivity::class.java)
        startActivity(intent)
    }

    private inner class SelectUsuariosDisposableObserver : DisposableObserver<List<UsuarioModel>>() {

        override fun onComplete() {

        }

        override fun onNext(t: List<UsuarioModel>) {
            muestraUsuarios(t)
        }

        override fun onError(e: Throwable) {
            muestraMensajeDeError(e)
        }
    }

    private fun muestraMensajeDeError(e: Throwable) {
        Toast.makeText(this, "Error consultando usuarios : ${e.message}", Toast.LENGTH_LONG).show()

    }


}
